#!/usr/bin/env python3

"""
The vcf2matrixEQTL_genotype is a tool that can convert a variant call format file with snps to a file that should
be accepted by the MatrixEQTL package in R.
"""

import glob
import os
import sys
import argparse
from _queue import Empty

import vcf
import pandas as pd

from multiprocessing import Process, freeze_support, Manager, Value, Lock

__author__ = "C.A. (Robert) Warmerdam"
__credits__ = ["Robert Warmerdam"]
__version__ = "0.0.2"
__status__ = "Prototype"


class ArgumentParser:
    def __init__(self):
        self.parser = self.create_argument_parser()

    def parse_input(self, argv):
        """
        Parse command line input.
        :param argv: given arguments
        :return: parsed arguments
        """
        args = self.parser.parse_args(argv)

        # Flatten list if there is input data.
        if hasattr(args, 'input_vcf_files'):
            args.input_vcf_files = [item for sublist in args.input_vcf_files for item in sublist]

        return args

    def create_argument_parser(self):
        """
        Method creating an argument parser
        :return: parser
        """
        parser = argparse.ArgumentParser(description=__doc__,
                                         formatter_class=argparse.RawDescriptionHelpFormatter)

        # Add argument for the number of processes to use for processing.
        parser.add_argument('-n', '--number_of_processes', required=False, type=int, default=os.cpu_count(),
                            help="The number of processes to start. (default is the os.cpu_count() output)")

        # Add argument flag for indicating gzipped data or not.
        parser.add_argument('-gz', '--gzipped', action='store_true', help="Indicates that the vcf files are gzipped")

        # Add argument flag for indicating that positional ids should be used instead of an index based ids.
        parser.add_argument('-p', '--use_positional_ids', action='store_true',
                            help="Indicates that positional snp-ids should be used instead of index based ids.")

        # Add argument flag that indicates if the number of alternate alleles should be used instead of
        # continuous alternate allele dosages.
        parser.add_argument('-d', '--use_discrete_dosages', action='store_true',
                            help="Indicates that rather than continuous alternate allele dosages, discrete alternate "
                                 "{0}allele dosages (the estimated sum of alternate alleles) should be reported."
                            .format(os.linesep))

        # Add argument for setting the size of chunks, in number of SNPs, in which SNPs will be stored before appending
        # them to the output files.
        parser.add_argument('-c', '--chunk_reading_size', type=int, required=False, default=50000,
                            help="The size of chunks, in number of SNPs, in which SNPs will be stored before appending "
                                 "{0}them to the output files. (default is 50000 SNPs)".format(os.linesep))

        # Add argument for a sample list file
        parser.add_argument('-s', '--sample_list_file', type=argparse.FileType('r'), required=True,
                            help="Give a file that lists the samples to use, separated by a linebreak.")

        # Add argument for an output prefix.
        parser.add_argument('-o', '--output_file_path_prefix', type=self.is_writable_location,
                            required=True, default=None,
                            help="Give a file path the output files can be written to. "
                                 "For example: 'd:\\files\\out'")

        # Add argument for filtering on the filter column
        parser.add_argument('-f', '--filter_ids', type=str,
                            required=False, default=["PASS"], nargs="+",
                            help="Specify the IDs from the filter field that should be present in a variant call."
                                 "The compulsory id should be separated by a space. (default is PASS)")

        # Add argument for vcf files.
        parser.add_argument('-i', '--input_vcf_files', type=self.is_data, required=True, nargs='+', default=None,
                            help="Give the paths to the .vcf (variant call format) files either separated by"
                                 "{0}spaces, or by using an asterisk as a wildcard for the globbing pattern."
                                 "{0}\tWhen using an asterisk, the argument 'd:\\files\\*.vcf'"
                                 "{0}\twill result in all .vcf files being parsed located inside the folder"
                                 "{0}\t'd:\\files'".format(os.linesep))
        return parser

    @staticmethod
    def is_readable_file(file_path):
        """
        Checks whether the given directory is readable
        :param file_path: a path to a file in string format
        :return: file_path
        :raises: Exception: if the given path is invalid
        :raises: Exception: if the given directory is not accessible
        """
        if not os.path.isfile(file_path):
            raise Exception("file path:{0} is not a valid file path".format(file_path))
        if os.access(file_path, os.R_OK):
            return file_path
        else:
            raise Exception("file path:{0} is not a readable file".format(file_path))

    def is_data(self, argument):
        """
        Parses a string that specifies the path(s) of a vcf file.
        :param argument: One or multiple file paths that represent all vcf files (use an asterisk, *).
        :return: A list with lists of vcf file paths.
        """
        # Expand using glob.
        paths = glob.glob(argument)
        files = []
        for file_path in paths:
            # Use default argparse filename parser for individual files.
            files.append(self.is_readable_file(file_path))
        if len(files) == 0:
            raise argparse.ArgumentTypeError(argument + ' matches exactly zero files.')
        return files

    @staticmethod
    def is_readable_dir(train_directory):
        """
        Checks whether the given directory is readable
        :param train_directory: a path to a directory in string format
        :return: train_directory
        :raises: Exception: if the given path is invalid
        :raises: Exception: if the given directory is not accessible
        """
        if not os.path.isdir(train_directory):
            raise Exception("directory: {0} is not a valid path".format(train_directory))
        if os.access(train_directory, os.R_OK):
            return train_directory
        else:
            raise Exception("directory: {0} is not a readable dir".format(train_directory))

    def is_writable_location(self, path):
        """
        Checks if the base of the given path represents a location in which writing is permitted.
        :param path: The path to check
        :return: The checked path
        """
        self.is_readable_dir(os.path.dirname(path))
        return path


class RecordValidator:
    """
    Class that should efficiently validate a record given a set of required filter field ids in the vcf file records.
    """

    def __init__(self, required_filter_field_ids):
        self.required_filter_field_ids = required_filter_field_ids
        self.is_valid_record = self._pick_validation_method()

    def _pick_validation_method(self):
        """
        Pre-picks the correct validation method.
        :return: the correct method for validating a record based on the required filter field ids.
        """
        if len(self.required_filter_field_ids) == 1 and "PASS" in self.required_filter_field_ids:
            return self._has_record_passed
        return self._has_required_filter_field_ids

    def _has_required_filter_field_ids(self, record):
        """
        Checks if the record filter field holds all the required filter field ids.
        :param record: The record to check.
        :return: true if the record filter field holds all the required filter field ids.
        """
        return all(required_id in record.FILTER for required_id in self.required_filter_field_ids)

    def _has_record_passed(self, record):
        """
        Checks if the records filter field has the "PASS" id or is empty.
        :param record: The record to check.
        :return: true if the records filter field has the "PASS" id or is empty.
        """
        return not record.FILTER or "PASS" in record.FILTER


class VcfFileProcessor:
    """
    Vcf file processor that outputs genetic variants per sample via a list of dicts, to a 'writer' object.
    """

    def __init__(self, writer,
                 snp_counter, gzipped, record_validator, chunk_reading_size, using_discrete_dosages, lock):
        self.lock = lock
        self.snp_counter = snp_counter
        self.writer = writer
        self.gzipped = gzipped
        self.snp_list = list()
        self.record_validator = record_validator
        self.chunk_reading_size = chunk_reading_size
        self.process_sample = self._pick_sample_processing_method(using_discrete_dosages)

    def process(self, vcf_file_path):
        """
        The method that performs processing. Per genetic variant a dict with information is stored in a list, which is
        outputted to a 'writer' object.
        """
        with open(vcf_file_path, 'rb' if self.gzipped else 'r') as opened:
            vcf_reader = vcf.Reader(opened)
            # Go through each record in the reader, counting the records.
            for i, record in enumerate(vcf_reader):
                # Process each record.
                try:
                    self.process_record(record)
                except KeyError as e:
                    raise SampleNotPresentException(e, os.path.basename(vcf_file_path))
                if (i + 1) % self.chunk_reading_size == 0:
                    # Process the snp list.
                    self.process_snp_list()
            # Process remaining SNPs.
            if len(self.snp_list) != 0:
                self.process_snp_list()

    def process_record(self, record):
        """
        Gets the information from a record and appends it to the list of genetic variants.
        :param record: The
        """
        if self.record_validator.is_valid_record(record):
            snp = {"chr": record.CHROM, "pos": record.POS}
            for sample in self.required_record_samples(record):
                snp.update({sample.sample: self.process_sample(sample)})
            self.snp_list.append(snp)

    def process_snp_list(self):
        """
        Outputs the snp list to a 'writer' object.
        """
        with self.lock:
            header = False
            # Output a header if the counter is still zero.
            if self.snp_counter.value == 0:
                header = True
            # Write the genetic variants to the 'writer' object.
            self.writer.write_genotype_data(pd.DataFrame(self.snp_list),
                                            header=header, start_index=self.snp_counter.value)
            # Add the written amount of genetic variants to the counter.
            self.snp_counter.value += len(self.snp_list)
            # Empty the counter
            self.snp_list = list()
            # Print the updated progress status
            print("\r{} SNPs processed".format(self.snp_counter.value), end="")

    def required_record_samples(self, record):
        """
        A method that lists the calls of a record of the required samples.
        :param record: The record to collect calls from.
        :return: a list with calls of a record of the required samples.
        """
        return [record.genotype(sample_id) for sample_id in self.writer.sample_list]

    def _get_aternate_allele_dosage_from_sample(self, sample):
        """
        Method that returns the dosage field from a sample
        :param sample: The VCF sample with a dosage field, DS.
        :return: A continuous value between 0 and 2, representing an alternate allele dosage between 0 and 2.
        """
        return sample['DS']

    def _get_estimated_alternate_allele_sum(self, sample):
        """
        Method that extracts the genotype field and counts the number of estimated alternate alleles in the genotype.
        :param sample: The VCF sample with a genotype, GT, field.
        :return: 0, 1 or 2, representing 0, 1 and 2 alternate alleles respectively.
        """
        return sample['GT'].count("1")

    def _pick_sample_processing_method(self, using_discrete_dosages):
        """
        Method that picks the correct method for extracting sample information.
        :param using_discrete_dosages: If the estimated sum of alternate allele dosages should be used.
        :return: The method that returns the required alternate allele information.
        """
        if using_discrete_dosages:
            return self._get_estimated_alternate_allele_sum
        return self._get_aternate_allele_dosage_from_sample


class MultiVcfFileProcessor:
    """
    A class that distributes vcf converting tasks among processes.
    """

    def __init__(self, writer, vcf_file_paths, cpu, gzipped, filter_ids, chunk_reading_size, using_discrete_dosages):
        self.using_discrete_dosages = using_discrete_dosages
        self.chunk_reading_size = chunk_reading_size
        self.filter_ids = filter_ids
        self.writer = writer
        self.gzipped = gzipped
        self.cpu = cpu
        manager = Manager()
        self.task_queue = manager.Queue()
        self.fill_queue(vcf_file_paths)

    def fill_queue(self, vcf_file_paths):
        """
        Fills the queue with file paths.
        :param vcf_file_paths: A list of file paths.
        """
        for path in vcf_file_paths:
            self.task_queue.put(path)

    def process_files(self):
        """
        Method that starts various processes to process the files in the task queue.
        """
        processes = list()
        # Make a thread save, shared mem, snp counter.
        snp_counter = Value('i', 0)
        lock = Lock()

        # Initialize vcf file processor
        file_processor = VcfFileProcessor(self.writer, snp_counter, self.gzipped,
                                          RecordValidator(self.filter_ids),
                                          self.chunk_reading_size, self.using_discrete_dosages, lock)

        # Build a set of processes and append these to a list.
        for i in range(self.cpu):
            process = Process(target=process_queue,
                              args=(self.task_queue, file_processor))
            process.start()
            processes.append(process)

        # Wait for the processes to finish.
        for process in processes:
            process.join()


class SampleNotPresentException(Exception):
    def __init__(self, exception, file):
        # Call the base class constructor with the parameters it needs
        super().__init__("One ore multiple samples were not present in file:{0}{1}{0}{2}"
                         .format(os.linesep, file, str(exception)))


class OutputWriter:
    """
    Class that is meant for writing a dataframe to a snp genotype file and another file with the snp locations.
    """

    def __init__(self, output_file_path_prefix, sample_list, use_positional_ids):
        # Add the correct suffices to the output prefix.
        self.use_positional_ids = use_positional_ids
        self.snp_file_path = output_file_path_prefix + "_snp.txt"
        self.snp_locations_file_path = output_file_path_prefix + "_snpsloc.txt"

        # Empty both files so new lines will not be appended to an existing file.
        open(self.snp_file_path, 'w').close()
        open(self.snp_locations_file_path, 'w').close()
        self.sample_list = sample_list

    def write_genotype_data(self, df, start_index=0, header=True):
        """
        Method that appends a dataframe to two files.
        :param df: The dataframe to write.
        :param start_index: The number to start the counting of snps with.
        :param header: If the header should be written.
        """
        # Check if there are cells None, which indicates that the sample names do not line up.
        if df.isnull().values.any():
            print("Warning: There are null values in the snp table", file=sys.stderr)

        # Add 'snpid' column
        df = df.assign(snpid=self.get_snpid_column(df, start_index))

        # Append the snps location data to the corresponding path.
        df.to_csv(self.snp_locations_file_path, mode='a', sep="\t",
                  columns=["snpid", "chr", "pos"],
                  index=False, header=header)

        # Append the snps sample data to the corresponding path.
        df.to_csv(self.snp_file_path, mode='a', sep="\t",
                  columns=["snpid"] + self.sample_list,
                  index=False, header=header)

    def get_snpid_column(self, df, start_index):
        """
        Method that generates the snpid column as an index or based on the location of the snp
        :param start_index: The index at which to start counting the snps.
        :param df: The dataframe to write.
        :return: A collection of snpids.
        """
        if self.use_positional_ids:
            # Get 'snpid' column to dataframe that looks like <chr-number>:<position>.
            return df.apply(lambda row: ":".join([str(row["chr"]), str(row["pos"])]), axis=1)
        # Get 'snpid' column to dataframe that counts starting from the init_dataframe_length + 1.
        return pd.Series(map(lambda x: "Snp_{}".format(x + start_index + 1), range(len(df))))


def get_sample_list(sample_list_file):
    """
    Function that reads a file with sample names separated by linebreaks to a list.
    :param sample_list_file: The opened sample list file.
    :return: a list with sample names.
    """
    return [line.strip() for line in sample_list_file]


def process_queue(task_queue, file_processor):
    """
    Empties a queue with vcf_file_paths by processing them.
    :param task_queue: A multiprocessing queue with vcf file paths.
    :param file_processor: An object that does the processing of a vcf file.
    """
    while not task_queue.empty():
        try:
            vcf_file = task_queue.get()
            file_processor.process(vcf_file)
        except Empty:
            # The task queue could suddenly be empty if an item is gotten just after the while loop returned True.
            pass


def main(argv=None):
    if argv is None:
        argv = sys.argv

    # Parse an input and output argument.
    arguments = ArgumentParser().parse_input(argv[1:])
    # Create an output writer.
    output_writer = OutputWriter(arguments.output_file_path_prefix, get_sample_list(arguments.sample_list_file),
                                 arguments.use_positional_ids)
    # Process the vcf files.
    MultiVcfFileProcessor(output_writer,
                          arguments.input_vcf_files,
                          arguments.number_of_processes,
                          arguments.gzipped,
                          arguments.filter_ids,
                          arguments.chunk_reading_size,
                          arguments.use_discrete_dosages).process_files()
    return 0


if __name__ == "__main__":
    freeze_support()
    sys.exit(main())
