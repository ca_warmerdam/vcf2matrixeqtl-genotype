# vcf2matrixEQTL_genotype

A new tool for converting variant call format (vcf) data to the format that should be 
accepted by the [MatrixEQTL](http://bios.unc.edu/research/genomic_software/Matrix_eQTL/) package in R.

## Getting started

Before you can run the program, make sure that the requirements are met.
The program requires python and open babel, both in 32-bit, or both in 64-bit.

* Install python (tested with version 3.7.2):

    * [Python Release 3.7.2](https://www.python.org/downloads/release/python-372/)

* Clone this program using git, or 
[download the source](https://bitbucket.org/ca_warmerdam/vcf2matrixeqtl-genotype/get/master.zip)
and extract it to a location you fancy.

* Open command prompt and change directory to this programs directory, where the `requirements.txt` file is located

* use `pip install -r requirements.txt`

## Usage

The script can be used as follows:

```
usage: vcf2matrixEQTL_genotype.py [-h] [-n NUMBER_OF_PROCESSES] [-gz] [-p]
                                  [-d] [-c CHUNK_READING_SIZE] -s
                                  SAMPLE_LIST_FILE -o OUTPUT_FILE_PATH_PREFIX
                                  [-f FILTER_IDS [FILTER_IDS ...]] -i
                                  INPUT_VCF_FILES [INPUT_VCF_FILES ...]

The vcf2matrixEQTL_genotype is a tool that can convert a variant call format file with snps to a file that should
be accepted by the MatrixEQTL package in R.

optional arguments:
  -h, --help            show this help message and exit
  -n NUMBER_OF_PROCESSES, --number_of_processes NUMBER_OF_PROCESSES
                        The number of processes to start. (default is the
                        os.cpu_count() output)
  -gz, --gzipped        Indicates that the vcf files are gzipped
  -p, --use_positional_ids
                        Indicates that positional snp-ids should be used
                        instead of index based ids.
  -d, --use_discrete_dosages
                        Indicates that rather than continuous alternate allele
                        dosages, discrete alternate allele dosages (the
                        estimated sum of alternate alleles) should be
                        reported.
  -c CHUNK_READING_SIZE, --chunk_reading_size CHUNK_READING_SIZE
                        The size of chunks, in number of SNPs, in which SNPs
                        will be stored before appending them to the output
                        files. (default is 50000 SNPs)
  -s SAMPLE_LIST_FILE, --sample_list_file SAMPLE_LIST_FILE
                        Give a file that lists the samples to use, separated
                        by a linebreak.
  -o OUTPUT_FILE_PATH_PREFIX, --output_file_path_prefix OUTPUT_FILE_PATH_PREFIX
                        Give a file path the output files can be written to.
                        For example: 'd:\files\out'
  -f FILTER_IDS [FILTER_IDS ...], --filter_ids FILTER_IDS [FILTER_IDS ...]
                        Specify the IDs from the filter field that should be
                        present in a variant call.The compulsory id should be
                        separated by a space. (default is PASS)
  -i INPUT_VCF_FILES [INPUT_VCF_FILES ...], --input_vcf_files INPUT_VCF_FILES [INPUT_VCF_FILES ...]
                        Give the paths to the .vcf (variant call format) files
                        either separated by spaces, or by using an asterisk as
                        a wildcard for the globbing pattern. When using an
                        asterisk, the argument 'd:\files\*.vcf' will result in
                        all .vcf files being parsed located inside the folder
                        'd:\files'
```

## Input data

The input that is required are vcf files (tested with version 4.1) and a file with
sample names to include in the output, separated with a newline.

## Output

The output of this program is described in the following examples:

* [genotype sample data](http://www.bios.unc.edu/research/genomic_software/Matrix_eQTL/Sample_Data/SNP.txt)

* [snp location sample data](http://www.bios.unc.edu/research/genomic_software/Matrix_eQTL/Sample_Data/snpsloc.txt)